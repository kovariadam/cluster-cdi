package cz.vutbr.fit.bp.xkovar57.serialization;

import com.thoughtworks.xstream.XStream;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Alternative;

/**
 * The ObjectSerializer implementation using XStream to serialize/deserialize objects into and from XML format.
 *
 * @author Adam Kovari <akovari @ redhat.com>
 */
@Alternative
@ApplicationScoped
public class XStreamObjectSerializer implements ObjectSerializer {
    private XStream xstream = new XStream();

    @Override
    public Object serialize(Object obj) throws InvalidTypeForSerializationException {
        return xstream.toXML(obj);
    }

    @Override
    public Object deserialize(Object obj) throws InvalidTypeForSerializationException {
        if (!(obj instanceof String)) {
            throw new InvalidTypeForSerializationException("Only String XML format can be deserialized, not: " + obj.getClass().getName());
        }

        return xstream.fromXML((String) obj);
    }
}
