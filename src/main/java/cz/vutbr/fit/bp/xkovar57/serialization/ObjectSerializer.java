package cz.vutbr.fit.bp.xkovar57.serialization;

/**
 * This simple interface must be implemented in order to allow a bean serialization and deserialization.
 *
 * @author Adam Kovari <akovari @ redhat.com>
 */
public interface ObjectSerializer {
    /**
     * Serialize an object.
     *
     * @param obj
     * @return serialized version of the object
     * @throws InvalidTypeForSerializationException
     */
    public Object serialize(Object obj) throws InvalidTypeForSerializationException;

    /**
     * Deserialize an object
     * @param obj
     * @return deserialized version of the object
     * @throws InvalidTypeForSerializationException
     */
    public Object deserialize(Object obj) throws InvalidTypeForSerializationException;
}
//TODO zistit ako classloader a porovnanie primitiv
