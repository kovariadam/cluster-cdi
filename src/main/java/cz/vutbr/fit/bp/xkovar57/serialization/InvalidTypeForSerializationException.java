package cz.vutbr.fit.bp.xkovar57.serialization;

/**
 * This exception is thrown when a data to be serialized or deserialized is not in the format
 * supported by the ObjectSerializer used in the deployment.
 *
 * @author Adam Kovari <akovari @ redhat.com>
 */
public class InvalidTypeForSerializationException extends Exception {
    public InvalidTypeForSerializationException() {
        super();
    }

    public InvalidTypeForSerializationException(String message) {
        super(message);
    }

    public InvalidTypeForSerializationException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidTypeForSerializationException(Throwable cause) {
        super(cause);
    }

    protected InvalidTypeForSerializationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
