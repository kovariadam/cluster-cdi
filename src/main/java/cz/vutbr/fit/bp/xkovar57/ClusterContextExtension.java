package cz.vutbr.fit.bp.xkovar57;

import cz.vutbr.fit.bp.xkovar57.annotations.ClusterScoped;
import cz.vutbr.fit.bp.xkovar57.common.CacheProvider;
import cz.vutbr.fit.bp.xkovar57.interceptors.ClusteredOperationInterceptor;
import org.apache.deltaspike.core.api.provider.BeanProvider;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.*;
import java.util.HashMap;
import java.util.Map;

/**
 * The main point of the extension, registers the @ClusteredContext into the CDI subsystem
 *
 * @author Adam Kovari <akovari @ redhat.com>
 */
public class ClusterContextExtension implements Extension {
    private Map<Class<?>, String> idMap;

    public ClusterContextExtension() {
        this.idMap = new HashMap<Class<?>, String>();
    }

    public void afterBeanDiscovery(@Observes AfterBeanDiscovery event, BeanManager beanManager) {
        event.addContext(new ClusterContext());
    }

    public void registerInterceptorBinding(@Observes BeforeBeanDiscovery event, BeanManager manager) {
        event.addAnnotatedType(manager.createAnnotatedType(ClusteredOperationInterceptor.class));
    }

    public void processBean(@Observes ProcessBean<?> event) {
        Bean<?> bean = event.getBean();

        if(ClusterScoped.class.equals(bean.getScope())) {
            PassivationCapable pc = (PassivationCapable) bean;

            idMap.put(bean.getBeanClass(), pc.getId());
        }
    }

    public Map<Class<?>, String> getIdMap() {
        return idMap;
    }
}
