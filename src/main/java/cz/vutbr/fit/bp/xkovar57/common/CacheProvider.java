package cz.vutbr.fit.bp.xkovar57.common;

import cz.vutbr.fit.bp.xkovar57.cache.CacheNotFoundException;
import cz.vutbr.fit.bp.xkovar57.cache.configuration.CacheConfigurator;
import cz.vutbr.fit.bp.xkovar57.serialization.InvalidTypeForSerializationException;
import cz.vutbr.fit.bp.xkovar57.serialization.ObjectSerializer;
import org.infinispan.Cache;
import org.infinispan.manager.CacheContainer;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class provides operations required for @CacheOperations. There are methods responsible for
 * maintaining a consistent state of a clustered been across the cluster nodes.
 *
 * @author Adam Kovari <akovari @ redhat.com>
 */
@ApplicationScoped
public class CacheProvider {
    private CacheContainer cacheContainer;
    private Cache<Object, Object> cache;
    private Logger logger = Logger.getLogger(this.getClass().getCanonicalName());

    @Inject
    private ObjectSerializer objectSerializer;

    @Inject
    private CacheConfigurator cacheConfigurator;

    @PostConstruct
    public void init() {
        cache = cacheConfigurator.configureCache();
    }

    /**
     * This method can be used to obtain the Cache instance.
     *
     * @return the Cache instance
     * @throws CacheNotFoundException when the Cache is not found
     */
    public Cache<Object, Object> getCache() throws CacheNotFoundException {
        if (cache == null) {
            throw new CacheNotFoundException("Cache is null");
        }
        return cache;
    }

    /**
     * This method saves an object to the shared Cache
     *
     * @param key
     * @param value
     * @return previously stored object
     * @throws IOException
     * @throws InvalidTypeForSerializationException
     */
    public Object storeIfAbsent(Object key, Object value) throws IOException, InvalidTypeForSerializationException {
        return cache.getAdvancedCache().putIfAbsent(key, objectSerializer.serialize(value));
    }

    /**
     * This method loads an Object from the Cache
     *
     * @param key
     * @return the object from the Cache
     */
    public Object load(Object key) {
        String retObj = (String) cache.get(key);

        if (retObj == null) {
            return null;
        }

        try {
            return objectSerializer.deserialize(retObj);
        } catch (InvalidTypeForSerializationException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }

        return null;
    }

    /**
     * This method updates the state of a clustered bean instance in the clustered storage. It is invoked on methods
     * annotated with @ClusteredOperation.
     *
     * @param key
     * @param target
     * @param method
     * @param args
     * @return the return value of a Method to be invoked
     * @throws IOException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws InvalidTypeForSerializationException
     */
    public Object update(Object key, Object target, Method method, Object[] args) throws IOException, InvocationTargetException, IllegalAccessException, InvalidTypeForSerializationException {
        boolean success = false;
        final int maxTries = 5;
        int i = 0;
        Object retVal = null;

        while (success != true && i < maxTries) {
            Object currentlyCachedVersion = cache.get(key);
            retVal = method.invoke(target, args);

            success = cache.replace(key, currentlyCachedVersion, objectSerializer.serialize(target));

            logger.finest("Replacing " + key + " with equals=" + currentlyCachedVersion.equals(objectSerializer.serialize(target)) + " and success=" + success);
            i++;
        }

        if (i == maxTries && !success) {
            logger.warning("Failed to update " + key + " after "+ i + " tries.");
        }

        return retVal;
    }
}
