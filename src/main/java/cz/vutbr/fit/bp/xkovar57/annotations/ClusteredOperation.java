package cz.vutbr.fit.bp.xkovar57.annotations;

import javax.interceptor.InterceptorBinding;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation can be applied on methods annotated as @ClusteredScoped
 * in order to cause the operation to be applied on the clustered version of the bean.
 *
 * Otherwise the operation is only performed on a local version of a bean.
 *
 * @author Adam Kovari <akovari @ redhat.com>
 */
@InterceptorBinding
@Target(value = {ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ClusteredOperation {
}
