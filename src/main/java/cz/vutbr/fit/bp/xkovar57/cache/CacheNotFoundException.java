package cz.vutbr.fit.bp.xkovar57.cache;

/**
 * An exception thrown when a Cache instance cannot be found or obtained.
 *
 * @author Adam Kovari <akovari @ redhat.com>
 */
public class CacheNotFoundException extends Exception {
    public CacheNotFoundException() {
    }

    public CacheNotFoundException(String message) {
        super(message);
    }

    public CacheNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public CacheNotFoundException(Throwable cause) {
        super(cause);
    }

    public CacheNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
