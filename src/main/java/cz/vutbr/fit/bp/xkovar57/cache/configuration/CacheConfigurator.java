package cz.vutbr.fit.bp.xkovar57.cache.configuration;

import org.infinispan.Cache;

/**
 * This simple interface must be implemented when a custom cache configuration is needed.
 *
 * @author Adam Kovari <akovari @ redhat.com>
 */
public interface CacheConfigurator {

    /**
     * Returns a cache
     *
     * @return a Cache instance
     */
    public Cache configureCache();
}
