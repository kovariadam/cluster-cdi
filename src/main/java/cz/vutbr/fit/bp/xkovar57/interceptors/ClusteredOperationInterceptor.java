package cz.vutbr.fit.bp.xkovar57.interceptors;

import cz.vutbr.fit.bp.xkovar57.ClusterContextExtension;
import cz.vutbr.fit.bp.xkovar57.annotations.ClusteredOperation;
import cz.vutbr.fit.bp.xkovar57.common.CacheProvider;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Logger;

/**
 * This interceptor is invoked whever a method annotated with @ClusteredOperation is invoked.
 * The purpose of the interceptor is to make sure that the state of the bean is consistent with the
 * rest of the cluster.
 *
 * @author Adam Kovari <akovari @ redhat.com>
 */
@Interceptor
@ClusteredOperation
public class ClusteredOperationInterceptor {
    @Inject
    private Instance<ClusterContextExtension> clusterContextExtension;

    @Inject
    private CacheProvider cacheProvider;

    private static ThreadLocal<Set<Class<?>>> previouslyInterceptedBean = new ThreadLocal<Set<Class<?>>>() {
        private AtomicReference<Set<Class<?>>> previouslyInterceptedBeanClass = new AtomicReference<Set<Class<?>>>();

        @Override
        public Set<Class<?>> get() {
            return previouslyInterceptedBeanClass.get();
        }

        @Override
        public void set(Set<Class<?>> value) {
            previouslyInterceptedBeanClass.set(value);
        }
    };

    @AroundInvoke
    public Object intercept(InvocationContext context) throws Exception {
        Logger logger = Logger.getLogger(this.getClass().getCanonicalName());

        Object target = context.getTarget();
        Method method = context.getMethod();

        if (isInterecptorBeingCalledRecursively(target)) {
            logger.finest("The interceptor is being called recursively. Skipping the invocation.");
            return context.proceed();
        }

        Object key = clusterContextExtension.get().getIdMap().get(target.getClass().getSuperclass());
        Object ret = null;

        if (key != null) {
            logger.finest("Retrieving object for key: " + key);

            ret = cacheProvider.update(key, target, method, context.getParameters());
        } else {
            logger.warning("Key for target class " + target.getClass() + " not found in " + clusterContextExtension.get().getIdMap() + ".");
        }

        previouslyInterceptedBean.get().clear();

        return ret;
    }

    private boolean isInterecptorBeingCalledRecursively(Object target) throws Exception {
        if (previouslyInterceptedBean.get() == null) {
            previouslyInterceptedBean.set(new HashSet<Class<?>>());
        }

        if (previouslyInterceptedBean.get().contains(target.getClass())) {
            return true;
        }
        previouslyInterceptedBean.get().add(target.getClass());
        return false;
    }
}
