package cz.vutbr.fit.bp.xkovar57;

import cz.vutbr.fit.bp.xkovar57.annotations.ClusterScoped;
import cz.vutbr.fit.bp.xkovar57.common.CacheProvider;
import cz.vutbr.fit.bp.xkovar57.serialization.InvalidTypeForSerializationException;
import org.apache.deltaspike.core.api.provider.BeanProvider;

import javax.enterprise.context.spi.Context;
import javax.enterprise.context.spi.Contextual;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.PassivationCapable;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Adam Kovari <akovari @ redhat.com>
 * @ClusterContext CDI implementation class
 */
public class ClusterContext implements Context {
    private Logger logger = Logger.getLogger(this.getClass().getCanonicalName());
    private CacheProvider cacheProvider;

    @Override
    public Class<? extends Annotation> getScope() {
        return ClusterScoped.class;
    }

    private CacheProvider getCacheProvider() {
        if (cacheProvider == null) {
            cacheProvider = BeanProvider.getContextualReference(CacheProvider.class, false);
        }

        return cacheProvider;
    }

    /**
     * Retrieves a new instance of a bean and saves it into the clustered storage
     *
     * @param contextual
     * @param creationalContext
     * @param <T>
     * @return the new instance of a bean
     */
    @Override
    public <T> T get(Contextual<T> contextual, CreationalContext<T> creationalContext) {
        try {
            Bean bean = (Bean) contextual;
            PassivationCapable pc = (PassivationCapable) bean;

            T obj = null;

            if (getCacheProvider().load(pc.getId()) == null) {
                obj = (T) (bean).create(creationalContext);

                T obj2 = (T) cacheProvider.storeIfAbsent(pc.getId(), obj);
                if (obj2 != null) {
                    obj = obj2;
                }
            }

            return obj;
        } catch (IOException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        } catch (InvalidTypeForSerializationException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }

        return null;
    }

    /**
     * Retrieves an existing instance of a bean from the clustered storage
     *
     * @param contextual
     * @param <T>
     * @return the instance of the bean
     */
    @Override
    public <T> T get(Contextual<T> contextual) {
        Bean bean = (Bean) contextual;
        PassivationCapable pc = (PassivationCapable) bean;

        T ret = (T) getCacheProvider().load(pc.getId());

        logger.finest("Loading " + pc.getId() + ": " + ret);

        return ret;
    }

    @Override
    public boolean isActive() {
        return true;
    }
}
