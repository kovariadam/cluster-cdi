package cz.vutbr.fit.bp.xkovar57.cache.configuration;

import org.infinispan.Cache;
import org.infinispan.manager.CacheContainer;
import org.jboss.solder.resourceLoader.Resource;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;

/**
 * An implementation of CacheConfiguration interface using JNDI to obtain the Cache instance.
 *
 * @author Adam Kovari <akovari @ redhat.com>
 */
@Alternative
@ApplicationScoped
public class JndiCacheConfigurator implements CacheConfigurator {
    @Inject
    @Resource("META-INF/cdi-cache-jndi.properties")
    private Properties configurationProperties;

    private final static String JNDI_NAME_PROPERTY = "jndi.name";
    private final static String CACHE_NAME_PROPERTY = "cache.name";

    @Override
    public Cache configureCache() {
        try {
            Context context = new InitialContext();
            CacheContainer cacheContainer = (CacheContainer) context.lookup(configurationProperties.getProperty(JNDI_NAME_PROPERTY));
            if (configurationProperties.contains(CACHE_NAME_PROPERTY)) {
                return cacheContainer.getCache(configurationProperties.getProperty(CACHE_NAME_PROPERTY));
            } else {
                return cacheContainer.getCache();
            }
        } catch (NamingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
