package cz.vutbr.fit.bp.xkovar57.annotations;

import javax.enterprise.context.NormalScope;
import java.lang.annotation.*;

/**
 * This annotation causes a bean to be treated as a clustered-wide bean.
 *
 * That means that operations on this beans can cause modifications to the bean state which is saved
 * in a cluster-wide context and all instances of an application server cluster can access the same bean state.
 *
 * @author Adam Kovari <akovari @ redhat.com>
 */
@Inherited
@NormalScope
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ClusterScoped {
}
