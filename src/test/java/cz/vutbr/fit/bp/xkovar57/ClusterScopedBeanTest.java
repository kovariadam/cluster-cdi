package cz.vutbr.fit.bp.xkovar57;

import cz.vutbr.fit.bp.xkovar57.annotations.ClusterScoped;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.descriptor.api.Descriptors;
import org.jboss.shrinkwrap.descriptor.api.beans10.BeansDescriptor;
import org.jboss.shrinkwrap.descriptor.api.spec.se.manifest.ManifestDescriptor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Properties;

/**
 * @author Adam Kovari <akovari @ redhat.com>
 */

@RunWith(Arquillian.class)
public class ClusterScopedBeanTest {
    @Deployment
    public static JavaArchive createDeployment() {
        BeansDescriptor beans = Descriptors.create(BeansDescriptor.class);
        ManifestDescriptor manifest = Descriptors.create(ManifestDescriptor.class);
        manifest.attribute("Dependencies", "org.infinispan export");

        JavaArchive jar = ShrinkWrap.create(JavaArchive.class)
                .addClass(ClusterContextExtension.class)
                .addClass(ClusterScoped.class)
                .addAsManifestResource(new StringAsset(beans.exportAsString()), "beans.xml")
                .addAsManifestResource(new StringAsset(manifest.exportAsString()), "MANIFEST.MF");

        return jar;
    }

    @Before
    public void setUp() throws Exception {
        Properties props = new Properties();
        props.put("infinispan.client.hotrod.server_list", "127.0.0.1:11222");
//        RemoteCacheManager manager = new RemoteCacheManager(props);
//        RemoteCache defaultCache = manager.getCache();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test1() {
//        Cache<String, Object> cache = dcm.getCache("infinispan/cdi-cluster");
//        cache.clear();
//        System.err.println("XXXXXXX: " + cache);
    }
}
